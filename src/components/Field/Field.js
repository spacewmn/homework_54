import React from 'react';
import Box from "../box/Box";
import './Field.css';

const Field = (props) => {
    const cells = props.field.map((box, index) => {
        return (
            <Box
                key={index}
                openCell={() => props.openCell(index)}
                isOpen={box.open}
                hasItem={box.hasItem}
            />
        )
    })

    return (
        <div className="Field">
            {cells}
        </div>
    );
};

export default Field;