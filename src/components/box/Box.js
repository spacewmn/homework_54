import React from 'react';
import './Box.css'

const Box = props => {
    const styleClasses = ['Box']
    const styleItem = []
    console.log(props.isOpen)
    if (props.isOpen) {
        styleClasses.push('Open');
    }
    if(props.hasItem){
        styleItem.push('Item');
    }

    return (
        <div className={styleClasses.join(' ')} onClick={props.openCell}>
            <span className={styleItem.join(' ')}/>
        </div>
    );
};

export default Box;