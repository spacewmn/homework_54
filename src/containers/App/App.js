import React, {useState} from 'react';
import './App.css';
import Field from "../../components/Field/Field";

const App = () => {

  const [gameArr, setGameArr] = useState([]);

  const generateCells = () => {
    const copyGameArr = [];

    const box = {
      hasItem: false,
      open: false,
    }

    for (let i = 0; i < 36; i++) {
      copyGameArr.push(box);
    }

    const index = Math.floor(Math.random() * 36);
    console.log(index)
    const obj = {...copyGameArr[index]}
    obj.hasItem = true;
    copyGameArr[index] = obj
    // console.log(copyGameArr[index])
    setGameArr(copyGameArr)
    console.log(copyGameArr);
  };

  if(gameArr.length === 0) {
    generateCells();
  }

  // if (useState([])) {
  //   addBox();
  // }
  //
  // const addO = () => {
  //
  // }

  const openCell = (ind) => {
    const copyGameArr = [...gameArr]
    const obj = {...copyGameArr[ind]}
    obj.open = true;
    copyGameArr[ind] = obj
    setGameArr(copyGameArr)
  }

  return (
    <div className="App">
      <Field
          field={gameArr}
          openCell={openCell}
      />
      <button onClick={generateCells}>Reset
      </button>
    </div>
  );
}

export default App;
